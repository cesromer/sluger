from django.conf.urls.defaults import patterns, include, url
from django.conf import settings
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
	url(r'^$', 'nota.views.index'),
	#url(r'^$', direct_to_template, {'template' : 'base.html'}),
	url(r'^admin/', include(admin.site.urls)),
    url(r'^ver/$', 'nota.views.index'),
	url(r'^ver/(?P<id_nota>\d+)/$', 'nota.views.ver'),
    # Examples:
    # url(r'^$', 'misapuntes.views.home', name='home'),
    # url(r'^misapuntes/', include('misapuntes.foo.urls')),
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    (r'css/(?P<path>.*)$', 'django.views.static.serve',{'document_root': settings.STATIC_ROOT + '/css'}),
    (r'images/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT + '/images'}),
    (r'js/(?P<path>.*)$', 'django.views.static.serve',{'document_root': settings.STATIC_ROOT + '/js'}),
)