from django.db import models
from datetime import datetime
# Create your models here.

class Category(models.Model):
	name = models.CharField(max_length = 60)
	def __unicode__(self):
		return self.name

class Nota(models.Model):
	title		= models.CharField(max_length = 120)
	body		= models.TextField()
	published	= models.DateTimeField(default = datetime.now)
	categories  = models.ManyToManyField(Category)

	def __unicode__(self):
		return self.title