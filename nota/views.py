# Create your views here.
from django.http import HttpResponse
from django.shortcuts import render_to_response
from nota.models import Nota

def index(request):
	try:
		notas = Nota.objects.all()
		ctx   = {'notas': notas}
	except notas.DoesNotExist:
		raise Http404
	return render_to_response('nota/index.html', ctx)


def ver(request, id_nota):
	try:
		n = Nota.objects.get(pk=id_nota)
	except notas.DoesNotExist:
		raise Http404
	return render_to_response('nota/ver.html',{'nota': n,'id_nota': id_nota})