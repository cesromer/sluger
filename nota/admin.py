from django.contrib import admin
from nota.models import * 

class NotaAdmin(admin.ModelAdmin):
	filter_horizontal = ('categories',)
	list_display = ('title','published')

admin.site.register(Category)
admin.site.register(Nota,NotaAdmin)